const express = require("express");
const app = express();
const Jimp = require("jimp");
const ColorThief = require("colorthief");
const { v4: uuidv4 } = require("uuid");
const fs = require("fs");

app.use(express.json());

app.all("/*", (req, res, next) => {
  // CORS headers - Set custom headers for CORS
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header(
    "Access-Control-Allow-Methods",
    "GET,PUT,POST,DELETE,OPTIONS,PATCH"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Content-type,Accept,X-Access-Token, vw-x-user-api-key-secret, vw-x-user-api-key, X-Vw-Anonymous-Id, X-Key, Cache-Control, X-Requested-With"
  );
  if (req.method === "OPTIONS") {
    res.status(200).end();
  } else {
    next();
  }
});

app.get("/getColors", (req, res) => {
  console.log("req");
  let { groupId, url, left, top, width, height, angle } = req.query;
  left = Number(left) >= 0 ? Number(left) : 0;
  top = Number(top) >= 0 ? Number(top) : 0;
  width = Number(width) >= 0 ? Number(width) : 0;
  height = Number(height) >= 0 ? Number(height) : 0;
  console.log(angle);
  let imageName;
  Jimp.read(url)
    .then((image) => {
      imageName = `${uuidv4()}.${image.getExtension()}`;
      // TODO: rotate the image properly
      image
        .rotate(Number(angle) || 0, false)
        .crop(left, top, width, height)
        .write(imageName);
    })
    .then(() => {
      return ColorThief.getPalette(imageName, 5);
    })
    .then((palette) => {
      res.json({ groupId, colors: palette });
      fs.unlink(imageName, (err) => {
        if (err) {
          console.log("error removing image", err);
        }
      });
    })
    .catch((err) => {
      console.log(err);
    });
});

app.listen(5000, () => {
  console.log("started listening on port 5000");
});
