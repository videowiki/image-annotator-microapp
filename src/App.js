import React from "react";
import Annotate from "./components/Annotate";

class App extends React.Component {
  render() {
    return <Annotate />;
  }
}

export default App;
